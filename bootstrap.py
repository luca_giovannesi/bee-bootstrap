import numpy as np
import matplotlib.pyplot as plt
import random
import scipy.stats
import sklearn
import scipy
import statistics
from matplotlib import cm

np.random.seed(1234)
random.seed(1234)

POPULATION_INFESTATION_LEVEL = 0.05
POPULATION_SIZE = 10000

population = []

int_infested_bees = int(POPULATION_INFESTATION_LEVEL*POPULATION_SIZE)
for i in range(POPULATION_SIZE):
    if i < int_infested_bees:
        population.append(True)
    else:
        population.append(False)

np.random.shuffle(population)


def get_error_paolo(acc_varroa:float, acc_healthy:float):

    def infestaction_rate(elements) -> float:
        n_tv = list(filter(lambda x: x, elements))
        n_ts = list(filter(lambda x: not x, elements))

        len_n_tv = len(n_tv)
        len_n_th = len(n_ts)

        n_est_health = acc_healthy*len_n_th + (1-acc_varroa)*len_n_tv
        n_est_varroa = (1-acc_healthy)*len_n_th + (acc_varroa)*len_n_tv

        #print("Estimated Varroa:",n_est_varroa)
        #infested = n_est_varroa#len(list(filter(lambda x: x, elements)))
        return n_est_varroa/(n_est_varroa+n_est_health)

    res = scipy.stats.bootstrap((population,), statistic=infestaction_rate, random_state=1234, n_resamples=200)
    return (res.confidence_interval.high-res.confidence_interval.low)*100
    #return res.standard_error

def get_error_luca(acc_varroa:float, acc_healthy:float):
    
#ax.hist(res.bootstrap_distribution,bins=50)
"""
values_x=[]
values_y=[]
values_z=[]
for i in np.arange(0.5,1,0.05):
    x_row=[]
    y_row=[]
    z_row=[]
    for j in np.arange(0.5,1,0.05):
        print(i,j)
        err=get_error(i,j)
        x_row.append(i)
        y_row.append(j)
        z_row.append(err)
    values_x.append(x_row)
    values_y.append(y_row)
    values_z.append(z_row)

print("X",values_x)
print("Y",values_y)
print("Z",values_z)

fig, ax = plt.subplots(subplot_kw={"projection": "3d"})


from matplotlib import cm
from matplotlib.ticker import LinearLocator
X = np.array(values_x)
Y = np.array(values_y)
Z = np.array(values_z)
print("SHAPE X:",X.shape, "SHAPE Y:",Y.shape, "SHAPE Z:",Z.shape)
# Plot the surface.
surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                       linewidth=0, antialiased=True)

# Customize the z axis.
ax.set_zlim(Z.min(), Z.max())
ax.zaxis.set_major_locator(LinearLocator(10))
# A StrMethodFormatter is used automatically
ax.zaxis.set_major_formatter('{x:.02f}')
ax.set(
    xlabel='Accuracy Varroa',
    ylabel='Accuracy Healthy',
    zlabel='Error',
)

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

plt.show()
"""