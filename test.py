import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
from matplotlib.lines import Line2D
import statistics
# Nuovi parametri
N = 1000
pi = 0.05

# Generazione della popolazione
np.random.seed(42)
infested_bees = int(N*pi)
population = np.array(['infestata'] * infested_bees + ['sana'] * (N-infested_bees))
np.random.shuffle(population)

# Conteggio delle api infestate e sane
num_infestate = np.sum(population == 'infestata')
num_sane = np.sum(population == 'sana')

def compute_error(acc_inf, acc_sane):

    # Calcolo dei tassi basati sull'accuratezza
    TP_inf = acc_inf
    FN_inf = 1 - acc_inf
    TN_sane = acc_sane
    FP_sane = 1 - acc_sane

    PAOLO=False

    if PAOLO:
        num_classified_sana = acc_sane*num_sane+(1-acc_inf)*num_infestate
        num_classified_infestata = (1-acc_sane)*num_sane+acc_inf*num_infestate
    else:
        infestata_classified = ((population == 'infestata') & (np.random.rand(N) < TP_inf)) | \
                            ((population == 'sana') & (np.random.rand(N) < FP_sane))
        sana_classified = ((population == 'sana') & (np.random.rand(N) < TN_sane)) | \
                        ((population == 'infestata') & (np.random.rand(N) < FN_inf))
        
        # Conteggio delle classificazioni
        num_classified_infestata = np.sum(infestata_classified)
        num_classified_sana = np.sum(sana_classified)

    # Stima del tasso di infestazione
    estimated_infestation_rate = num_classified_infestata / num_classified_infestata + num_classified_sana

    # Parametri del bootstrap
    n_iterations = 100
    bootstrap_estimates = []
    sample_size = 200

    # Esecuzione del bootstrap
    for _ in range(n_iterations):
        # Campionamento con sostituzione
        bootstrap_sample = np.random.choice(population, size=sample_size, replace=True)
        
        if PAOLO:
            a = (1-acc_sane)*np.sum(bootstrap_sample == 'sana')+acc_inf*np.sum(bootstrap_sample == 'infestata')
            estimated_infestation_rate = a / sample_size
        else:
            # Applicazione dei classificatori
            infestata_classified = ((bootstrap_sample == 'infestata') & (np.random.rand(sample_size) < TP_inf)) | \
                                ((bootstrap_sample == 'sana') & (np.random.rand(sample_size) < FP_sane))
            # Stima del tasso di infestazione
            estimated_infestation_rate = np.sum(infestata_classified) / sample_size

        bootstrap_estimates.append(estimated_infestation_rate)

    # Calcolo dell'intervallo di confidenza
    ci_lower = np.percentile(bootstrap_estimates, 2.5)
    ci_upper = np.percentile(bootstrap_estimates, 97.5)
    estimated_infestation_rate = statistics.mean(bootstrap_estimates)

    print("(",acc_sane,acc_inf,") NUM_EST_INFESTED:", num_classified_infestata,"NUM_EST_SANE:", num_classified_sana,"EST_INFEST_RATE:" ,estimated_infestation_rate ,"INTERVAL:(",ci_lower,"-",ci_upper,")")
    return estimated_infestation_rate,ci_lower,ci_upper


def generate_3d():
    values_x=[]
    values_y=[]
    values_z=[]
    for i in np.arange(0.8,1,0.01):
        x_row=[]
        y_row=[]
        z_row=[]
        for j in np.arange(0.8,1,0.01):
            print(i,j)
            est_ir,l,h=compute_error(i,j)
            x_row.append(i)
            y_row.append(j)
            z_row.append(est_ir)
        values_x.append(x_row)
        values_y.append(y_row)
        values_z.append(z_row)

    print("X",values_x)
    print("Y",values_y)
    print("Z",values_z)

    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})


    from matplotlib import cm
    from matplotlib.ticker import LinearLocator
    X = np.array(values_x)
    Y = np.array(values_y)
    Z = np.array(values_z)
    print("SHAPE X:",X.shape, "SHAPE Y:",Y.shape, "SHAPE Z:",Z.shape)
    # Plot the surface.
    surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
                        linewidth=0, antialiased=True)

    # Customize the z axis.
    ax.set_zlim(Z.min(), Z.max())
    ax.zaxis.set_major_locator(LinearLocator(10))
    # A StrMethodFormatter is used automatically
    ax.zaxis.set_major_formatter('{x:.02f}')
    ax.set(
        xlabel='Accuracy Varroa (X)',
        ylabel='Accuracy Healthy (Y)',
        zlabel='Error (Z)',
    )

    # Add a color bar which maps values to colors.
    fig.colorbar(surf, shrink=0.5, aspect=5)

    plt.show()

def generate_multiline():
    healthy_accuracies = np.arange(0.9,1.01,0.01)
    cmap = plt.cm.viridis
    norm = mcolors.Normalize(vmin=min(healthy_accuracies), vmax=max(healthy_accuracies))
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)

    for health_accuracy in healthy_accuracies:
        for varroa_accuracy in np.arange(0.90,1.01,0.01):
            est_ir,l,h=compute_error(varroa_accuracy,health_accuracy)
            step = health_accuracy/25
            plt.plot([varroa_accuracy+step, varroa_accuracy+step], [l, h], color=sm.to_rgba(health_accuracy))
            # plt.plot(varroa_accuracy+step, est_ir, 'x', color=sm.to_rgba(health_accuracy))

    plt.axhline(y=pi, color='#ff00ff', linestyle='-', label="Real rate")
    lines = [Line2D([0], [0], color=sm.to_rgba(c), linewidth=3, linestyle='-') for c in healthy_accuracies]
    labels = ["Healthy_acc %f" % c for c in healthy_accuracies]
    lines.append(Line2D([0], [0], color="#ff00ff", linewidth=3, linestyle='-') )
    labels.append("True Value IR")
    plt.legend(lines,labels)
    plt.xlabel("Varroa accuracy")
    plt.ylabel("Infestation Rate") 
    plt.show()

# generate_3d()
generate_multiline()